"""
This module provides funcitonality for the EPCC Datamart API AWS Lambda handler
author: 'team daedalus'
"""

from __future__ import print_function
from urllib.parse import urlparse
import os
import importlib
import json
import boto3
from botocore.client import Config

class EpccMartStateManager(object):
    SAMPLE_EVENT = {
        "Records":[
            {
                "EventSource":"aws:sns",
                "EventVersion":"1.0",
                "EventSubscriptionArn":"arn:aws:sns:us-east-1:297450527080:epcc-mart-state-manager-sns:b47f2a04-6f83-4ac0-acf6-6d181037c94d",
                "Sns":{
                    "Type":"Notification",
                    "MessageId":"946ffb58-4a78-528a-881a-a3448a8e96e1",
                    "TopicArn":"arn:aws:sns:us-east-1:297450527080:epcc-mart-state-manager-sns",
                    "Subject":"tester",
                    "Message":"{}",
                    "Timestamp":"2020-11-04T14:46:14.080Z",
                    "SignatureVersion":"1",
                    "Signature":"lLckEEZmLy3vvJEbCGm/NRg33K/PyE8Mv9OGZRjpqzIu8lw6U4Nu+pSNaAzqGf2yxRPgQawRR6FQ5beu54HSKfSYPVVqLHDal+qdlOaLEIlEJaiydP06Yp+/b7SIZd+8smE+NLIoGWMOjHQ2w1D5nGFyO1uJtONw6a+VAnt3JmRw0s4raf2hDC9ld8YelTFDSesJXhEUBnfMjDDwiu2kvnOLvfJxCke5sm/I4rRrnxooox6KzBo87KK+JbzufYAtGsV2z+X3rwhGOfK0KUHpY5ZOTkXskBwFMa2Nj8RFwBbJf1wg+XmiBuV+Fm9io/7V3auP+VwAWQNVQiuFzrQRsw==",
                    "SigningCertUrl":"https://sns.us-east-1.amazonaws.com/SimpleNotificationService-a86cb10b4e1f29c941702d737128f7b6.pem",
                    "UnsubscribeUrl":"https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:297450527080:epcc-mart-state-manager-sns:b47f2a04-6f83-4ac0-acf6-6d181037c94d",
                    "MessageAttributes":{
                    
                    }
                }
            }
        ]
    }

    def __init__(self):
        '''
        This function executes when object is instantiated.
        '''

    @staticmethod
    def lambda_handler(event=None, context=None):
        '''
        This function is the primary state manager for epcc-data-mart
        '''

        try: 
            if event:
                print("\n*** event ***\n")
                print(event)
            if context:
                print("\n*** context ***\n")
                print(context)

                if 'raise' in context:
                    raise(eval(context['raise']))

            return True
            
        except Exception as e:

            print("Bad kitty.. notify here?  %s" % e)

            return False

if __name__ == '__main__':

    MART_STATE_MGR = EpccMartStateManager()
    MART_STATE_MGR.lambda_handler(MART_STATE_MGR.SAMPLE_EVENT, None)
