import unittest
from unittest.mock import MagicMock, patch
import os
import inspect
import json
import importlib
MART = importlib.import_module('api.epcc_mart_api_handler')


class TestEpccMartMethods(unittest.TestCase):

    ERR_RESPONSE_MUST_BE_TYPE_DICT = 'response must be of type: dict'
    ERR_RESPONSE_MUST_BE_TYPE_STR = 'response must be of type: str'
    ERR_BODY_MUST_BE_TYPE_STR = 'response.body must be of type: str'
    ERR_INPUT_MUST_BE_TYPE_DICT = 'input event must be of type: dict'
    INT_PREFIX = '/int/'

    # SET UP 1X PER CLASS #
    @classmethod
    def setUpClass(cls):
        # Set up data for the whole TestCase
        cls.ApiTestClass = MART.EpccMartApi()

        cls.assertIsNone(cls, cls.ApiTestClass.event, 'event is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.path, 'path is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.method, 'method is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.output_data, 'output_data is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.query_params, 'query_params is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.error_data, 'error_data is not None on class init')
        cls.assertIsNone(cls, cls.ApiTestClass.success_data, 'success_data is not None on class init')

    # LAMBDA_HANDLER TEST #
    def test_lambda_handler(self):
        handler_result = MART.lambda_handler(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertIsInstance(json.loads(handler_result['body']), str, self.ERR_BODY_MUST_BE_TYPE_STR)

    # TEST EXECUTE API #
    def test_execute_api(self):
        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertIsInstance(json.loads(handler_result['body']), str, self.ERR_BODY_MUST_BE_TYPE_STR)

    # TEST RAISE ERROR #
    def test_raise_error(self):
        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, {'error': 'NameError'})
        self.assertEqual(json.loads(handler_result['body']), 'NameError', 'based on context, should have raised NameError')

    # API STATUS API EXECUTE TEST #
    def test_api_status_api_path(self):
        uri_path_to_test = 'api/status'
        method_to_test = 'GET'
        MART.SAMPLE_EVENT['path'] = '/' + uri_path_to_test
        MART.SAMPLE_EVENT['pathParameters']['proxy'] = uri_path_to_test
        MART.SAMPLE_EVENT['requestContext']['path'] = self.INT_PREFIX + uri_path_to_test
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertEqual(handler_result['body'], '"OK"', 'api/status GET should return OK')

    # MART-META API PUT EXECUTE TEST #
    def test_mart_meta_api_path(self):
        uri_path_to_test = 'mart-meta'
        method_to_test = 'PUT'
        output_data_to_test = {'martMeta':
                            {'tenantName': 'daedulas-tenant',
                             'martName': 'coolmart',
                             'martTableName': 'even_cooler_table_name',
                             'martDescription': 'one heck of a mart table',
                             'martTableColumnList': ['c1', 'c2', 'c3', 'c4', 'c5']
                             }
                        }

        MART.SAMPLE_EVENT['path'] = '/' + uri_path_to_test
        MART.SAMPLE_EVENT['pathParameters']['proxy'] = uri_path_to_test
        MART.SAMPLE_EVENT['requestContext']['path'] = self.INT_PREFIX + uri_path_to_test
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['body'] = output_data_to_test

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertEqual(handler_result['statusCode'], 200, 'mart-meta PUT should return statusCode: 200')

        # MART META VALIDATION TEST #

        method_to_test = 'GET'
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertEqual(handler_result['body'], '{"martMeta": {"martName": "mart-stub", "martId": "1000", "secret": "fartypuss"}}', 'mart-meta GET should return martMeta stub')

    # INVALID API ENDPOINT TEST #
    def test_invalid_api_path(self):
        uri_path_to_test = 'bologna-cheese'
        method_to_test = 'PUT'
        MART.SAMPLE_EVENT['path'] = '/' + uri_path_to_test
        MART.SAMPLE_EVENT['pathParameters']['proxy'] = uri_path_to_test
        MART.SAMPLE_EVENT['requestContext']['path'] = self.INT_PREFIX + uri_path_to_test
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertEqual(handler_result['body'], '"Unknown or unsupported request"', 'invalid path should return Unknown/Unsupported')

    # RESPONSE SUCCESS METHOD TEST #
    def test_response_success(self):
        self.api_success_resp = self.ApiTestClass.response_success(data='TEST SUCCESS')

        self.assertIsInstance(self.api_success_resp, dict, self.ERR_RESPONSE_MUST_BE_TYPE_DICT)

        status_code = self.api_success_resp.get('statusCode')
        self.assertEqual(status_code, 200, 'statusCode was not 200')

        resp_msg = self.api_success_resp.get('body')
        self.assertIsInstance(resp_msg, str, self.ERR_BODY_MUST_BE_TYPE_STR)

        self.assertTrue(self.ApiTestClass.success_data)

    # RESPONSE FAIL METHOD TEST #
    def test_response_fail(self):
        self.api_fail_resp = self.ApiTestClass.response_fail()

        self.assertIsInstance(self.api_fail_resp, dict, self.ERR_RESPONSE_MUST_BE_TYPE_DICT)

        status_code = self.api_fail_resp.get('statusCode')
        self.assertEqual(status_code, 500, 'statusCode was not 500')

        resp_msg = self.api_fail_resp.get('body')
        self.assertIsInstance(resp_msg, str, self.ERR_BODY_MUST_BE_TYPE_STR)

        self.assertEqual(self.ApiTestClass.error_data.strip('"'), 'Unexpected error on API request', '`error_data` var != expected')

    # UNKNOWN ENDPOINT TEST CALLS response_fail #
    def test_unknown_api_triggers_response_fail(self):
        uri_path_to_test = 'did-it-response_fail'
        method_to_test = 'GET'
        MART.SAMPLE_EVENT['path'] = '/' + uri_path_to_test
        MART.SAMPLE_EVENT['pathParameters']['proxy'] = uri_path_to_test
        MART.SAMPLE_EVENT['requestContext']['path'] = self.INT_PREFIX + uri_path_to_test
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test

        self.ApiTestClass.response_fail = MagicMock(name='response_fail', wraps=self.ApiTestClass.response_fail)
        self.ApiTestClass.response_success = MagicMock(name='response_success', wraps=self.ApiTestClass.response_success)

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertTrue(self.ApiTestClass.response_fail.called, 'response_fail was not called on invalid API request path')
        self.assertFalse(self.ApiTestClass.response_success.called, 'response_success was called on invalid API request path')
        self.assertEqual(self.ApiTestClass.response_fail.call_args[1]['error_data'], 'Unknown or unsupported request', 'unknown URL should call response_fail with data = Unknown/Unsupported')
        self.assertEqual(handler_result['body'], '"Unknown or unsupported request"', 'unknown URL should return body = Unknown/Unsupported')

    # VALID ENDPOINT TEST CALLS response_success #
    def test_valid_api_triggers_response_success(self):
        uri_path_to_test = 'api/status'
        method_to_test = 'GET'
        MART.SAMPLE_EVENT['path'] = '/' + uri_path_to_test
        MART.SAMPLE_EVENT['pathParameters']['proxy'] = uri_path_to_test
        MART.SAMPLE_EVENT['requestContext']['path'] = self.INT_PREFIX + uri_path_to_test
        MART.SAMPLE_EVENT['httpMethod'] = method_to_test
        MART.SAMPLE_EVENT['requestContext']['httpMethod'] = method_to_test

        self.ApiTestClass.response_fail = MagicMock(name='response_fail', wraps=self.ApiTestClass.response_fail)
        self.ApiTestClass.response_success = MagicMock(name='response_success', wraps=self.ApiTestClass.response_success)

        handler_result = self.ApiTestClass.execute_api(MART.SAMPLE_EVENT, MART.SAMPLE_EVENT.get('requestContext'))

        self.assertIsInstance(MART.SAMPLE_EVENT, dict, self.ERR_INPUT_MUST_BE_TYPE_DICT)
        self.assertTrue(self.ApiTestClass.response_success.called, 'response_success was not called on valid API request path')
        self.assertFalse(self.ApiTestClass.response_fail.called, 'response_fail was called on valid API request path')
        self.assertEqual(self.ApiTestClass.response_success.call_args[0][0], 'OK', 'valid URL should call response_success with data = OK')
        self.assertEqual(handler_result['body'], '"OK"', 'valid URL should return body = OK')

    @patch.dict(os.environ, {"LambdaEnvironment": "LOCAL_DEV"})
    def test_environment_vars_local_dev(self):
        epcc_environments = ['LOCAL_DEV', 'D1UNIT', 'Q1UNIT', 'P1UNIT']
        self.assertIn(os.environ.get('LambdaEnvironment'), epcc_environments, 'env variable `LambdaEnvironment` is not a valid option')

    def test_environment_vars_none(self):
        self.assertIsNone(os.environ.get('LambdaEnvironment', None), 'env variable `LambdaEnvironment` should be None')


if __name__ == '__main__':

    unittest.main()
