import unittest
from unittest.mock import MagicMock, patch
from unittest import mock
import os
import inspect
import json
import importlib
STATE_MGR = importlib.import_module('state_manager.epcc_mart_state_manager')

@mock.patch.dict(os.environ, {"LambdaEnvironment": "LOCAL_DEV"})
class TestEpccMartMethods(unittest.TestCase):

    LAMBDA_HANDLER_DID_NOT_RETURN_TRUE = 'lambda_handler did not return True'
    LAMBDA_HANDLER_DID_NOT_RETURN_FALSE = 'lambda_handler did not return False on exception'

    # SET UP 1X PER CLASS #
    @classmethod
    def setUpClass(cls):
        # Set up data for the whole TestCase
        cls.ApiTestClass = STATE_MGR.EpccMartStateManager()
        cls.assertIsNotNone(cls.ApiTestClass, 'EpccMartStateManager() initialized')

    def test_call_lambda_handler_test_opt_params(self):

        return_value = self.ApiTestClass.lambda_handler(self.ApiTestClass.SAMPLE_EVENT, {'context': 'this is my context'})
        self.assertTrue(return_value, self.LAMBDA_HANDLER_DID_NOT_RETURN_TRUE)

        return_value = self.ApiTestClass.lambda_handler()
        self.assertTrue(return_value, self.LAMBDA_HANDLER_DID_NOT_RETURN_TRUE)

class TestEpccNameMain(unittest.TestCase):

    MODULE_NAME__NAME__ = 'module_name.__name__'

    # SET UP 1X PER CLASS #
    @classmethod
    def setUpClass(cls):
        # Set up data for the whole TestCase
        pass

    def test_init_w_name_eq_main(self):
        from state_manager.epcc_mart_state_manager import EpccMartStateManager
        mock.patch.object(EpccMartStateManager, self.MODULE_NAME__NAME__, '__main__')
        self.ApiTestClass = EpccMartStateManager()
        self.ApiTestClass.lambda_handler = MagicMock(name='lambda_handler')
        self.assertIsNotNone(self.ApiTestClass, 'EpccMartStateManager() initialized w name == main')

    def test_init_w_name_not_eq_main(self):
        from state_manager.epcc_mart_state_manager import EpccMartStateManager
        mock.patch.object(EpccMartStateManager, self.MODULE_NAME__NAME__, 'state_manager.epcc_mart_state_manager')
        self.ApiTestClass = EpccMartStateManager()
        self.ApiTestClass.lambda_handler = MagicMock(name='lambda_handler')
        self.assertIsNotNone(self.ApiTestClass, 'EpccMartStateManager() initialized w name == state_manager.epcc_mart_state_manager')

    def test_lambda_handler_w_err_context(self):
        from state_manager.epcc_mart_state_manager import EpccMartStateManager
        self.ApiTestClass = EpccMartStateManager()
        lambda_handler_response = self.ApiTestClass.lambda_handler(None, {'raise': 'NameError'})
        self.assertFalse(lambda_handler_response, 'lambda_handler_response did not return false on exception')


if __name__ == '__main__':

    unittest.main()
