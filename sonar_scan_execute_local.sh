sonar-scanner \
  -Dsonar.projectKey=epcc-mart-backend \
  -Dsonar.projectName=epcc-mart-backend \
  -Dsonar.sources=./api,./state_manager \
  -Dsonar.tests=./unit_tests \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.user=admin \
  -Dsonar.password=admin \
  -Dsonar.python.coverage.reportPaths=coverage.xml \
  -Dsonar.exclusions=coverage.xml \
  -Dsonar.projectVersion=1.0 \
  -Dsonar.scm.provider=git \

