"""
This module provides functionality for the EPCC Datamart API AWS Lambda handler
author: 'team daedalus'
"""

from __future__ import print_function
from urllib.parse import urlparse
import os
import importlib
import json
import boto3
from hashlib import blake2b
import re
from botocore.client import Config


class EpccMartApi(object):
    """Base Class module for epcc-mart api"""
    event = None
    path = None
    method = None
    output_data = None
    query_params = None
    error_data = None
    success_data = None

    def __init__(self):
        """
        base poller class init
        :return: None
        """
        self.event = None
        self.path = None
        self.method = None
        self.output_data = None
        self.query_params = None
        self.error_data = None
        self.success_data = None
        self.mart_file = None
        self.s3bucket = 'epcc-mart-service-integration'
        self.s3prefix = 'mart-meta/'


    def response_success(self, data=None):
        """
        generates properly formed Lambda proxy success response
        """

        self.success_data = data
        response = {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": "*",
            },
            "body": json.dumps(data, default=str)
        }
        print(response)
        return response

    def response_fail(self, error_data='Unexpected error on API request'):
        """
        generates properly formed Lambda proxy fail response
        """

        error_string = json.dumps(error_data, default=str)
        self.error_data = error_string

        response = {
            "statusCode": 500,
            "headers": {
                "Access-Control-Allow-Origin": "*",
            },
            "body": error_string
        }
        print(response)
        return response

    def copy_to_s3(self):
        """
        cp file to s3 location
        :return: None
        """

        try:
            session = boto3.Session(profile_name='epcc-poc', region_name='us-east-1')
            s3 = session.client('s3')
            self.mart_file = '_'.join([self.output_data['martMeta'].get('tenantName'), self.output_data['martMeta'].get('martName'), 'meta.json'])
            with open(self.mart_file, 'wb') as f:
                f.write(json.dumps(self.output_data).encode())

            with open(f.name, 'rb') as upload_file:
                resp = s3.upload_fileobj(upload_file, Bucket=self.s3bucket, Key=self.s3prefix+self.mart_file)

            try:
                os.remove(f.name)
            except OSError:
                pass

            return resp
        except Exception as e:
            raise e

    def validate_mart_meta(self):
        """
        Test that I must specify the name of the mart table.
        Test that I may specify a description for the mart tables
        Test that the description may not exceed 2000 characters.
        Test that I must specify a list of columns to include in the mart table.
        Test that I may not include duplicate column names in my mart table definition.
        Test that the table name and column names may only contain characters in the set { a-z, 0-9, ''} _i.e. lowercase alpha characters, digits, or underscore
        Test that the table name and column names must begin with a lowercase alpha character.
        """
        try:

            if not self.output_data['martMeta'].get('tenantName'):
                raise ValueError('tenantName is required input for mart table creation')
            if not self.output_data['martMeta'].get('martName'):
                raise ValueError('martName is required input for mart table creation')
            if not self.output_data['martMeta'].get('martTableName'):
                raise ValueError('martTableName is required input for mart table creation')
            if not self.output_data['martMeta'].get('martDescription'):
                raise ValueError('martDescription is required input for mart table creation')
            if not len(self.output_data['martMeta'].get('martDescription')) <= 2000:
                raise ValueError('martDescription is required input for mart table creation')
            if not len(self.output_data['martMeta'].get('martTableColumnList')) >= 1:
                raise ValueError('martTableColumnList is required input of type list > 1 for mart table creation')
            if not len(self.output_data['martMeta'].get('martTableColumnList')) == len(set(self.output_data['martMeta'].get('martTableColumnList'))):
                raise ValueError('martTableColumnList must be list of unique column names')
            if not re.search(r'^[a-z0-9_]*$', self.output_data['martMeta'].get('martTableName')):
                raise ValueError('martTableName must contain only `^[a-z0-9_]*$]` characters')
            if not re.match(r'[a-z]', self.output_data['martMeta'].get('martTableName')):
                raise ValueError('martTableName must begin with lowercase alpha character')
            if [i for i in self.output_data['martMeta'].get('martTableColumnList') if not re.search(r'^[a-z0-9_]*$', i)]:
                raise ValueError('martTableColumnList values must contain only `^[a-z0-9_]*$` characters')
            if [i for i in self.output_data['martMeta'].get('martTableColumnList') if not re.match(r'[a-z]', i)]:
                raise ValueError('martTableColumnList values must begin with lowercase alpha character')

            hash_input = self.output_data['martMeta'].get('tenantName')
            hash_input += self.output_data['martMeta'].get('martName')
            hash_input += self.output_data['martMeta'].get('martTableName')
            mart_id = blake2b(hash_input.encode(), digest_size=5).hexdigest()

            self.output_data['martMeta']['martId'] = mart_id

            return True

        except Exception as e:
            raise e

    def execute_api(self, event=None, context=None):
        """
        This function is the business logic handler for the Datamart API
        """

        try:
            self.event = event
            print("\n*** event ***\n")
            print(event)

            if context and 'error' in context:
                err_to_raise = eval(context['error'])
                raise(err_to_raise(context['error']))

            self.query_params = event.get('queryStringParameters', {})
            self.method = event['httpMethod']
            self.path = event['path']
            self.output_data = None

            if self.path == '/api/status' and self.method == 'GET':
                # This block handles GET requests for: /api/status
                self.output_data = 'OK'

            if self.path == '/mart-meta':
                if self.method == 'PUT':
                    # This block handles PUT requests for: /mart-meta
                    self.output_data = {'martMeta':
                                            {'tenantName': 'daedulas-tenant',
                                             'martName': 'coolmart',
                                             'martTableName': 'even_cooler_table_name',
                                             'martDescription': 'one heck of a mart table',
                                             'martTableColumnList': ['c1', 'c2', 'c3', 'c4', 'c5']
                                             }
                                        }
                    #self.output_data = event.get('body')

                    self.validate_mart_meta()
                    self.copy_to_s3()

                if self.method == 'GET':
                    # This block handles PUT requests for: /mart-meta
                    resp = {'martMeta': {'martName': 'mart-stub', 'martId': '1000', 'secret': 'fartypuss'}}
                    self.output_data = resp

            if not self.output_data:
                return self.response_fail(error_data=str('Unknown or unsupported request'))

            return self.response_success(self.output_data)

        except Exception as e:

            return self.response_fail(error_data=str(e))


def lambda_handler(event, context):
    """Instantiates class and triggers execution method.
    """
    api_class = EpccMartApi()
    resp = api_class.execute_api(event, context)
    return resp


### ALLOW LOCAL DEV AND UNIT TESTS TO USE THE STUB EVENT ###

if os.environ.get('LambdaEnvironment') == 'LOCAL_DEV' or not os.environ.get('LambdaEnvironment'):
    DOMAIN_NAME = "8vkodkpaw6.execute-api.us-east-1.amazonaws.com"
    USER_AGENT = "PostmanRuntime/7.26.5"

    SAMPLE_EVENT = {
        "resource":"/{proxy+}",
        "path":"/mart-meta",
        "httpMethod":"PUT",
        "headers":{
            "Accept":"*/*",
            "Accept-Encoding":"gzip, deflate, br",
            "Cache-Control":"no-cache",
            "CloudFront-Forwarded-Proto":"https",
            "CloudFront-Is-Desktop-Viewer":"true",
            "CloudFront-Is-Mobile-Viewer":"false",
            "CloudFront-Is-SmartTV-Viewer":"false",
            "CloudFront-Is-Tablet-Viewer":"false",
            "CloudFront-Viewer-Country":"US",
            "Host": DOMAIN_NAME,
            "Postman-Token":"a81c8acc-91c3-4f25-84d5-957e7b97c95b",
            "User-Agent": USER_AGENT,
            "Via":"1.1 94d0933da2f9125873922490cab8e2b7.cloudfront.net (CloudFront)",
            "X-Amz-Cf-Id":"u0Ee9hFPddFBwFe9YEyg2ao8rom1FGrXO5r8BVRsaQw4jW1WhDRTjQ==",
            "X-Amzn-Trace-Id":"Root=1-5f9f1b79-7972b14d7dd216811882b577",
            "x-api-key":"DD8321938c2e06d31f3a2530ce9662be2a",
            "X-Forwarded-For":"159.127.85.254, 130.176.11.178",
            "X-Forwarded-Port":"443",
            "X-Forwarded-Proto":"https"
        },
        "multiValueHeaders":{
            "Accept":[
                "*/*"
            ],
            "Accept-Encoding":[
                "gzip, deflate, br"
            ],
            "Cache-Control":[
                "no-cache"
            ],
            "CloudFront-Forwarded-Proto":[
                "https"
            ],
            "CloudFront-Is-Desktop-Viewer":[
                "true"
            ],
            "CloudFront-Is-Mobile-Viewer":[
                "false"
            ],
            "CloudFront-Is-SmartTV-Viewer":[
                "false"
            ],
            "CloudFront-Is-Tablet-Viewer":[
                "false"
            ],
            "CloudFront-Viewer-Country":[
                "US"
            ],
            "Host":[
                DOMAIN_NAME
            ],
            "Postman-Token":[
                "a81c8acc-91c3-4f25-84d5-957e7b97c95b"
            ],
            "User-Agent":[
                USER_AGENT
            ],
            "Via":[
                "1.1 94d0933da2f9125873922490cab8e2b7.cloudfront.net (CloudFront)"
            ],
            "X-Amz-Cf-Id":[
                "u0Ee9hFPddFBwFe9YEyg2ao8rom1FGrXO5r8BVRsaQw4jW1WhDRTjQ=="
            ],
            "X-Amzn-Trace-Id":[
                "Root=1-5f9f1b79-7972b14d7dd216811882b577"
            ],
            "x-api-key":[
                "DD8321938c2e06d31f3a2530ce9662be2a"
            ],
            "X-Forwarded-For":[
                "159.127.85.254, 130.176.11.178"
            ],
            "X-Forwarded-Port":[
                "443"
            ],
            "X-Forwarded-Proto":[
                "https"
            ]
        },
        "queryStringParameters":"None",
        "multiValueQueryStringParameters":"None",
        "pathParameters":{
            "proxy":"api/status"
        },
        "stageVariables":"None",
        "requestContext":{
            "resourceId":"64rre4",
            "resourcePath":"/{proxy+}",
            "httpMethod":"GET",
            "extendedRequestId":"VWE7CHZMIAMF-eA=",
            "requestTime":"01/Nov/2020:20:32:57 +0000",
            "path":"/int/api/status",
            "accountId":"297450527080",
            "protocol":"HTTP/1.1",
            "stage":"int",
            "domainPrefix":"8vkodkpaw6",
            "requestTimeEpoch":1604262777776,
            "requestId":"93178f68-a1f6-4059-899e-4032b74b7376",
            "identity":{
                "cognitoIdentityPoolId":"None",
                "cognitoIdentityId":"None",
                "apiKey":"DD8321938c2e06d31f3a2530ce9662be2a",
                "principalOrgId":"None",
                "cognitoAuthenticationType":"None",
                "userArn":"None",
                "apiKeyId":"dsyzzryh3k",
                "userAgent": USER_AGENT,
                "accountId":"None",
                "caller":"None",
                "sourceIp":"bogusSourceIp",
                "accessKey":"None",
                "cognitoAuthenticationProvider":"None",
                "user":"None"
            },
            "domainName": DOMAIN_NAME,
            "apiId":"8vkodkpaw6"
        },
        "body":"TEST_EVENT",
        "isBase64Encoded":False
    }

if __name__ == '__main__':
    lambda_handler(SAMPLE_EVENT, SAMPLE_EVENT['requestContext'])
