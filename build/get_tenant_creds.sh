
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
echo \
    '''
    -----------------------------------------------------------------------------------------------------
    THIS SCRIPT GENERATES AWS CREDENTIALS FROM THE SKYNET API GIVEN A TENANT CODE
    -----------------------------------------------------------------------------------------------------

    Example: sh get_tenant_creds.sh D1UNIT
    
    where 
    -1st argument is the the Tenant Code  (ie..'D1UNIT')

    the output will be written to a file called tenant-credential.txt
    -----------------------------------------------------------------------------------------------------
    '''
  exit 0
fi

TENANT_CODE=$1

echo "TENANT_CODE=$TENANT_CODE"

echo "Get Config parameters info"
configData="`cat config_params_data.txt`"
if [ -z "$configData" ]; then
	exit 1
fi

SKYNET_API_URL=$(echo "$configData" | jq -r '. | .SKYNET_API_URL')
SKYNET_API_BASIC_AUTH_STRING=$(echo "$configData" | jq -r '. | .SKYNET_API_BASIC_AUTH_STRING')
SKYNET_USER_NAME=$(echo "$configData" | jq -r '. | .SKYNET_USER_NAME')
SKYNET_USER_PWD=$(echo "$configData" | jq -r '. | .SKYNET_USER_PWD')
ENVIRONMENT=$(echo "$configData" | jq -r '. | .ENVIRONMENT')
TEMP_CREDENTIAL_EXPIRY_IN_MINUTES=$(echo "$configData" | jq -r '. | .TEMP_CREDENTIAL_EXPIRY_IN_MINUTES')

echo "----vars-----"
echo $SKYNET_API_URL
echo $SKYNET_API_BASIC_AUTH_STRING
echo $SKYNET_USER_NAME
echo $SKYNET_USER_PWD
echo $ENVIRONMENT
echo $TEMP_CREDENTIAL_EXPIRY_IN_MINUTES
echo "------------"

echo "Get Tenant Level Credentials.. "
echo "Get skynet token"
tokenResult=$(curl -X POST "${SKYNET_API_URL}/api/v1/authorization/tokens" \
-H "Content-Type: application/x-www-form-urlencoded" \
-H "Accept-Language: en-US" \
-H "Authorization: Basic ${SKYNET_API_BASIC_AUTH_STRING}" \
-H "Environment: ${ENVIRONMENT}" \
-d "grant_type=password&username=${SKYNET_USER_NAME}&password=${SKYNET_USER_PWD}&response_type=token")

echo $tokenResult

accessToken=$(echo "$tokenResult" | jq -r '.|.AccessToken')
echo $accessToken
if [ -z "$accessToken" ] || [ "$accessToken" == "null" ]; then
	exit 1
fi
echo $tokenResult 

echo "Get skynet tenant credential"
tenantCredentialResult=$(curl -X POST "${SKYNET_API_URL}/api/v1/tenants/credentials/cloud?tenantCode=${TENANT_CODE}&expiryInMinutes=${TEMP_CREDENTIAL_EXPIRY_IN_MINUTES}" \
-H "Accept-Language: en-US" \
-H "Authorization: OAuth $accessToken") 
echo $tenantCredentialResult
if [ -z "$tenantCredentialResult" ]; then
	exit 1
fi
errorCode=$(echo "$tenantCredentialResult" | jq -r '.|.ErrorCode')
if [ ! -z "$errorCode" ] && [ "$errorCode" != "null" ]; then
	exit 1
fi

if [ $TENANT_CODE = 'SHARED' ]
then 
  echo $tenantCredentialResult > shared-credential.txt
else
  echo $tenantCredentialResult > tenant-credential.txt
fi