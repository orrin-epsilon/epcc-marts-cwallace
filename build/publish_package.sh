
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
echo \
    '''
    -----------------------------------------------------------------------------------------------------
    THIS SCRIPT DEPLOYS A TERRAFORM 
    -----------------------------------------------------------------------------------------------------

    Example: sh build/publish_package.sh epsilon-unite-product com.epsilon.unite epcc-mart-service-api epcc_mart_service_api epcc-mart-service-alpha-20.04.02.0094.zip
    
    where 
    -1st argument is the the sonar nexus repo (ie..'epsilon-unite-product')
    -2nd argument is the "group" which translates into the folder structure even though delimited with "." (ie..'com.epsilon.unite')
    -3rd is the package folder in nexus (ie.. 'epcc-mart-service') 
    -4th is the zip package name (ie. 'epcc_mart_service')
    -5th is the stage 'alpha, beta, release')

    Note Nexus Credentials, publish versions are based on GoCD environment variables.  You may need to edit this script for local testing
    -----------------------------------------------------------------------------------------------------
    '''
  exit 0
fi

PUBLISH_REPO=$1
PUBLISH_GROUP=$2
PUBLISH_FOLDER=$3
PUBLISH_PACKAGE=$4
STAGE=$5

if [ -z "$STAGE" ]
then
  STAGE='alpha'
fi

PUBLISH_VERSION=$STAGE-$BASE_VERSION${GO_PIPELINE_COUNTER}

#CI uses some environment variables that CD may have an analogous var for.

if [ -z "$NEXUS_REPO_URL" ] 
then
  NEXUS_REPO_URL=$GO_REPO_UNITE_EPCC_MART_SERVICE_REPO_URL
fi

if [ -z "$BASE_VERSION" ] #CI uses this environment variable, if not get create a CD version
then
  PUBLISH_VERSION=$(echo $GO_PACKAGE_UNITE_EPCC_MART_SERVICE_VERSION | sed -e "s/alpha/$STAGE/g" | sed -e "s/beta/$STAGE/g")
fi

if [ -z "$NEXUS_USER" ] #CI uses this environment variable, if not get create a CD version
then
  NEXUS_USER=$GO_REPO_UNITE_EPCC_MART_SERVICE_USERNAME
  NEXUS_PASSWORD=$GO_REPO_UNITE_EPCC_MART_SERVICE_PASSWORD
fi


#CD beta and release have some suffix's appended to the folder
if [ $STAGE = 'beta' ]
then 
    FOLDER_SUFFIX='-dev'
elif [ $STAGE = 'release' ]
then 
    FOLDER_SUFFIX='-qa'
fi


#At this point should have everything needed to publish
echo "-------PUBLISH--------"

echo publish_version: $PUBLISH_VERSION

echo destination: $PUBLISH_FOLDER$FOLDER_SUFFIX

$(curl -X POST \
$NEXUS_REPO_URL/service/rest/v1/components?repository=$PUBLISH_REPO \
-H "accept: application/json" -H "Content-Type: multipart/form-data" \
-v -u $NEXUS_USER:$NEXUS_PASSWORD \
-F "maven2.groupId=$PUBLISH_GROUP" \
-F "maven2.artifactId=$PUBLISH_FOLDER$FOLDER_SUFFIX" \
-F "maven2.version=$PUBLISH_VERSION" \
-F "maven2.asset1=@$PUBLISH_PACKAGE.zip;type=application/x-zip-compressed" \
-F "maven2.asset1.extension=zip")

res=$?

if test "$res" != "0"; 
then
    echo "the curl command failed with: $res"
    exit 1
fi

echo "-------DONE-------"

