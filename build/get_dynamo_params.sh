
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
echo \
    '''
    -----------------------------------------------------------------------------------------------------
    THIS SCRIPT GETS CONFIG DATA FROM A DYNAMOB INSTANCE IN EPSILON-ENTERPRISE-SOURCECONTROL (724058771651)
    -----------------------------------------------------------------------------------------------------

    Example: sh get_dynamo_params.sh DEV_AU_GOCD_AUTOMATION_CONFIG
    
    where 
    -1st argument is the the DynamoDB Table  (ie..'DEV_AU_GOCD_AUTOMATION_CONFIG')

    the output will be written to a file called config_params_data.txt
    -----------------------------------------------------------------------------------------------------
    '''
  exit 0
fi

GOCD_CONFIG_DYNAMODB_TABLE=${1}

echo "Get required Config Parameters from dynamodb item"

echo "********************************"

echo "**** Fetching Shared parmeters ****"
SPRINT_VERSION=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-SPRINT_VERSION\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $SPRINT_VERSION

NEXUS_API_USER_NAME=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-NEXUS_API_USER_NAME\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $NEXUS_API_USER_NAME

NEXUS_API_PASSWORD=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-NEXUS_API_PASSWORD\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $NEXUS_API_PASSWORD

SKYNET_USER_NAME=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-SKYNET_USER_NAME\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $SKYNET_USER_NAME

SKYNET_API_URL=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-SKYNET_API_URL\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $SKYNET_API_URL

SKYNET_API_BASIC_AUTH_STRING=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-SKYNET_API_BASIC_AUTH_STRING\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $SKYNET_API_BASIC_AUTH_STRING

SKYNET_USER_PWD=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-SKYNET_USER_PWD\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $SKYNET_USER_PWD

ENVIRONMENT=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-ENVIRONMENT\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $ENVIRONMENT

TEMP_CREDENTIAL_EXPIRY_IN_MINUTES=$(aws dynamodb get-item --region us-east-1 --table-name ${GOCD_CONFIG_DYNAMODB_TABLE} --key "{\"ConfigId\": {\"S\": \"SHARED-TEMP_CREDENTIAL_EXPIRY_IN_MINUTES\"}}" | jq -r '. | .Item.ConfigValue.S')
echo $TEMP_CREDENTIAL_EXPIRY_IN_MINUTES

echo "**** Completed Fetching Tenant Specific parmeters ****"

configData="{
\"SPRINT_VERSION\":\"$SPRINT_VERSION\",
\"NEXUS_API_USER_NAME\":\"$NEXUS_API_USER_NAME\",
\"NEXUS_API_PASSWORD\":\"$NEXUS_API_PASSWORD\",
\"SKYNET_USER_NAME\":\"$SKYNET_USER_NAME\",
\"SKYNET_API_URL\":\"$SKYNET_API_URL\",
\"SKYNET_API_BASIC_AUTH_STRING\":\"$SKYNET_API_BASIC_AUTH_STRING\",
\"SKYNET_USER_PWD\":\"$SKYNET_USER_PWD\",
\"ENVIRONMENT\":\"$ENVIRONMENT\",
\"TEMP_CREDENTIAL_EXPIRY_IN_MINUTES\":\"$TEMP_CREDENTIAL_EXPIRY_IN_MINUTES\" 
}"
echo $configData
echo $configData> config_params_data.txt