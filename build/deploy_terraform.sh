if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
echo \
    '''
    -----------------------------------------------------------------------------------------------------
    THIS SCRIPT DEPLOYS A TERRAFORM 
    -----------------------------------------------------------------------------------------------------

    Example: sh build/deploy_terraform.sh 'terraform/*.tf' integration AKIAICQ2YQOSYYXZ7QPQ X3eXCvw6Z1WXFasdxQRy3PU5K/sqPRtcvcNHUR07 

    
    where 
    -1st argument is the the local relative location of the .tf file or files (ie..'terraform/*.tf or terraform/joesucks.tf')
    -2nd argument in the environment/tenant code (ie.. 'integration or D1UNIT')
    -3nd is the AWS key
    -4th is the AWS secret
    -5th is the AWS session token (optional in integration)

    -----------------------------------------------------------------------------------------------------
    '''
  exit 0
fi

TFLOCAL=$1
TFTENANT=`echo $2 | tr '[:upper:]' '[:lower:]'`
KEY=$3
SECRET=$4
TOKEN=$5

cp $TFLOCAL .

BUCKET=`echo epcc-mart-service-$TFTENANT` 

#alias date='gdate' #for local/mac. note require GNU gdate for this to work
TIMESTAMP=$(date +%N)

TFSTAGE = 'prod'
if [ $TFTENANT = 'integration' ]
then 
    TFSTAGE='int'
elif [ $TFTENANT = 'q2unit' ]
then 
    TFSTAGE='dev'
elif [ $TFTENANT = 'q1unit' ]
then 
    TFSTAGE='qa'
fi

#create environment variables for terraform
export TF_VAR_aws_key=$KEY
export TF_VAR_aws_secret=$SECRET
export TF_VAR_aws_session_token=$TOKEN
export TF_VAR_aws_session_token=$TOKEN
export TF_VAR_epcc_mart_environment=$TFTENANT
export TF_VAR_api_stage_name=$TFSTAGE

if AWS_ACCESS_KEY_ID=$KEY AWS_SECRET_ACCESS_KEY=$SECRET AWS_SESSION_TOKEN=$TOKEN aws s3 ls "s3://$BUCKET" 2>&1 | grep -q 'NoSuchBucket'
then

    echo "No existing $BUCKET found indicating virgin account... we should be safe to deploy here without an existing terraform state file."

else

    echo "Found existing $BUCKET bucket! Getting the last terraform file."

    AWS_ACCESS_KEY_ID=$KEY AWS_SECRET_ACCESS_KEY=$SECRET AWS_SESSION_TOKEN=$TOKEN aws s3 cp s3://$BUCKET/terraform/terraform.tfstate terraform.tfstate --quiet

    if [ ${PIPESTATUS[0]} -ne 0 ]; 
    then 
        echo "No AWS access and/or no existing terraform state files exists. Exiting as this should not be the case!"
        exit 1
    else    
        echo "AWS Access confirmed and found a existing terraform.tfstate file in S3 to use."
    fi

fi;

#init folder and validate config... fail out if bad
echo 'terraform init'
terraform init
if [ ${PIPESTATUS[0]} -ne 0 ]; then echo "error on terraform init!"; rm -f *.tf; rm -f *.tfstate*; rm -f -rf .terraform; exit 1; fi;

echo 'terraform validate'
terraform validate
if [ ${PIPESTATUS[0]} -ne 0 ]; then echo "error on terraform validate!"; rm -f *.tf; rm -f *.tfstate*; rm -f -rf .terraform; exit 1; fi;

echo 'terraform apply'
terraform apply -auto-approve
if [ ${PIPESTATUS[0]} -ne 0 ]; then echo "error on terraform apply!"; exit 1; fi;

echo 'copying tfstate to s3'
AWS_ACCESS_KEY_ID=$KEY AWS_SECRET_ACCESS_KEY=$SECRET AWS_SESSION_TOKEN=$TOKEN aws s3 mv terraform.tfstate s3://$BUCKET/terraform/terraform.tfstate
AWS_ACCESS_KEY_ID=$KEY AWS_SECRET_ACCESS_KEY=$SECRET AWS_SESSION_TOKEN=$TOKEN aws s3 mv terraform.tfstate.backup s3://$BUCKET/terraform/backup/$TIMESTAMP/terraform.tfstate.backup

# #clean up
rm -f *.tf; 
rm -f *.tfstate*;
rm -f -rf .terraform; 