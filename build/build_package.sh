if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
echo \
    '''
    -----------------------------------------------------------------------------------------------------
    THIS SCRIPT CREATES A LAMBDA PACKAGE USING A FOLDER INPUT
    -----------------------------------------------------------------------------------------------------

    Example: 'sh build/build_package.sh epcc_mart_api_handler api' 
    
    where 
    -1st argument is package name which should be the same as the handler.py (ie..'epcc_mart_api_handler')
    -2nd argument is folder relative to this script ('api')

    -----------------------------------------------------------------------------------------------------
    '''
  exit 0
fi

set -e 

PACKAGE=$1
FOLDER=$2

echo "creating the $PACKAGE lambda package from the $FOLDER folder"

CUR_DIR=$PWD

echo $CUR_DIR

pip3 install -t deployment-package/$FOLDER -r $FOLDER/requirements.txt
cp $FOLDER/$PACKAGE.py deployment-package/$FOLDER/$PACKAGE.py
cd deployment-package/$FOLDER/
zip $PACKAGE.zip *
cp $PACKAGE.zip $CUR_DIR/$PACKAGE.zip
cd $CUR_DIR
rm -rf deployment-package