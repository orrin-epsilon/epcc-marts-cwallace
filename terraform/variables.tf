#these are the default variables used in the build locally and when not defined.  
#Note, GoCD will be overriding these for each environment we deploy to.

#this is the environment the build is occurring in, and it drives the S3 bucket name
variable "epcc_mart_environment" {
}

#this is aws key provided to deploy_terraform.sh
variable "aws_key" {
}

#this is aws secret provided to deploy_terraform.sh
variable "aws_secret" {
}

#this is aws session token optionally provided to deploy_terraform.sh
variable "aws_session_token" {
}

#this api stage name, which will be int, dev, qa, prod based on the tenant environment
variable "api_stage_name" {
}