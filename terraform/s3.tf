provider "aws" {
  version = "3.13.0"
  region  = "us-east-1"
  access_key = var.aws_key
  secret_key = var.aws_secret
  token = var.aws_session_token
}

resource "aws_s3_bucket" "epcc-mart-service-environment" {
  bucket = "epcc-mart-service-${var.epcc_mart_environment}"
  acl    = "private"
  tags = {
  }
}