resource "aws_lambda_function" "epcc-mart-api-handler" {

  filename = "epcc_mart_api_handler.zip"
  function_name = "epcc-mart-api-handler"
  handler = "epcc_mart_api_handler.lambda_handler"
  runtime = "python3.6"
  role = aws_iam_role.epcc-mart-service-role.arn
  source_code_hash = filebase64sha256("epcc_mart_api_handler.zip")

  environment {
    variables = {
      LambdaEnvironment = "AWS_ENVIRONMENT"
    }
  }

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_api_gateway_resource" "proxy" {
   rest_api_id = aws_api_gateway_rest_api.epcc-mart-rest-api.id
   parent_id   = aws_api_gateway_rest_api.epcc-mart-rest-api.root_resource_id
   path_part   = "{proxy+}"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_api_gateway_method" "proxy" {
   rest_api_id   = aws_api_gateway_rest_api.epcc-mart-rest-api.id
   resource_id   = aws_api_gateway_resource.proxy.id
   http_method   = "ANY"
   authorization = "NONE"
   api_key_required = true

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_lambda_permission" "epcc-mart-gw-invoke" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.epcc-mart-api-handler.function_name
   principal     = "apigateway.amazonaws.com"

   source_arn = "${aws_api_gateway_rest_api.epcc-mart-rest-api.execution_arn}/*/*"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]
  
}