resource "aws_lambda_function" "epcc-mart-state-manager" {

  filename = "epcc_mart_state_manager.zip"
  function_name = "epcc-mart-state-manager"
  handler = "epcc_mart_state_manager.lambda_handler"
  runtime = "python3.6"
  role = aws_iam_role.epcc-mart-service-role.arn
  source_code_hash = filebase64sha256("epcc_mart_state_manager.zip")

  environment {
    variables = {
      LambdaEnvironment = "AWS_ENVIRONMENT"
    }
  }

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_sns_topic" "epcc-mart-state-manager-sns" {
    name = "epcc-mart-state-manager-sns"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_sns_topic_subscription" "epcc-mart-state-manager-sns-subscription" {
  topic_arn = aws_sns_topic.epcc-mart-state-manager-sns.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.epcc-mart-state-manager.arn

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_sns_topic_policy" "default" {
  arn    = aws_sns_topic.epcc-mart-state-manager-sns.arn
  policy = data.aws_iam_policy_document.sns_topic_policy.json

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [aws_sns_topic.epcc-mart-state-manager-sns.arn]
  }

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_cloudwatch_event_rule" "epcc-mart-state-heartbeat" {
  name                = "epcc-mart-state-heartbeat"
  description         = "Start the epcc-mart state manager at consistent intervals"
  schedule_expression = "cron(0/15 * * * ? *)"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_cloudwatch_event_target" "epcc-mart-state-event-target" {
  rule      = aws_cloudwatch_event_rule.epcc-mart-state-heartbeat.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.epcc-mart-state-manager-sns.arn

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_lambda_permission" "epcc-mart-state-manager-sns" {
  statement_id  = "epcc-mart-stage-manager-sns-invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.epcc-mart-state-manager.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.epcc-mart-state-manager-sns.arn

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]
}