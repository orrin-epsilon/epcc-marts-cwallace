resource "aws_iam_role" "epcc-mart-service-role" {
   name = "epcc-mart-service-role"
   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "events.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_iam_role_policy" "epcc-mart-service-policy" {
  name = "epcc-mart-service-policy"
  role = aws_iam_role.epcc-mart-service-role.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "apigateway:*",
          "cloudfront:*",
          "cloudwatch:*",               
          "iam:*",
          "lambda:*",
          "logs:*",
          "redshift:*",
          "s3:*",
          "sns:*",
          "ssm:*",
          "sts:*",
          "events:*"
        ],
        "Effect": "Allow",
        "Resource": "*"
      }
    ]
  }
  EOF

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}