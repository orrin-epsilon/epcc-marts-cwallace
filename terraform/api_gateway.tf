resource "aws_api_gateway_rest_api" "epcc-mart-rest-api" {
  name        = "epcc-mart-rest-api"
  description = "Rest API for the EPCC Mart"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_api_gateway_integration" "lambda" {
   rest_api_id = aws_api_gateway_rest_api.epcc-mart-rest-api.id
   resource_id = aws_api_gateway_method.proxy.resource_id
   http_method = aws_api_gateway_method.proxy.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.epcc-mart-api-handler.invoke_arn

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_api_gateway_deployment" "epcc-mart-gateway-deployment" {

   rest_api_id = aws_api_gateway_rest_api.epcc-mart-rest-api.id
   stage_name  = "${var.api_stage_name}"

  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_s3_bucket.epcc-mart-service-environment
  ]

}


resource "aws_api_gateway_api_key" "epcc-mart-api-key" {
  name = "epcc-mart-api-key"
  description = "this is the api key for the epcc-mart api"
  value = "DD8321938c2e06d31f3a2530ce9662be2a"

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

resource "aws_api_gateway_usage_plan" "epcc-mart-usage-plan" {
  name = "epcc-mart-usage-plan"

  api_stages {
    api_id = aws_api_gateway_rest_api.epcc-mart-rest-api.id
    stage  = "${var.api_stage_name}"
  }

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}


resource "aws_api_gateway_usage_plan_key" "epcc-mart-api-key-plan" {
  key_id        = aws_api_gateway_api_key.epcc-mart-api-key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.epcc-mart-usage-plan.id

  depends_on = [
    aws_s3_bucket.epcc-mart-service-environment
  ]

}

output "base_url" {
  value = aws_api_gateway_deployment.epcc-mart-gateway-deployment.invoke_url
}
