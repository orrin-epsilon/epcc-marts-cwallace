# README #

### What is this repository for? ###

* This repo is for all humans
* The epc.customer repo to handle the backend logic for the creation of user defined data marts that source from the client's data warehouse (python)


### Contribution guidelines ###

* To start work, developers are required to make a fork of this repo and work within that "dev-user" fork.
dev-forks will be merged into the trunk via pull requests. And should be done so as frequently as is
responsibly prudent


### How do I get set up? ###

* python 3.6
* aws cli
* consult requirements.txt